import matplotlib.pyplot as plt

def Square(Num_List):
    return [num**2 for num in Num_List]

def Cube(Num_List):
    return [num**3 for num in Num_List]

def Plot_Graph(x,y,fig = 1, title = '', xlabel = '', ylabel = ''):
    plt.figure(fig)
    plt.plot(x,y)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)


input_list = range(1,11)
squares = Square(input_list)
cubes = Cube(input_list)

Plot_Graph(input_list,squares,xlabel='Numbers', ylabel='Square',)
Plot_Graph(input_list,cubes,fig=2,xlabel='Numbers', ylabel='Cube',)
#display the graph
plt.show()