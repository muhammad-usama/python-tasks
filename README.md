# Python Tasks

# How to use this repo:
This repo contain 9 tasks.

In the top right corner click on the blue button saying "Code", copy the URL given under the option "Clone with HTTPS"
In your linux terminal, go to some directory where you want to work from and write git clone <insert URL>

# Task 1

# Description:
- This program will find all such numbers which are divisible by 7 but are not a multiple of 5, between 2000 and 3200 (both included)
- To run this program write **python3 task1.py** on the terminal.

