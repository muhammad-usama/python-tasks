import argparse
import random
import json
import os

def Estimate_pi(Iteration):
    hits = 0
    for val in range(Iteration):
        x = random.random()
        y = random.random()
        if (x**2 + y**2) < 1:
            hits += 1
    print('Estimated value of pi:',4*hits/Iteration)

def Parse_Arguments():
    parser = argparse.ArgumentParser(description='Monte Carlo’s simulation')
    exclusive_group = parser.add_mutually_exclusive_group(required=True)
    exclusive_group.add_argument('-i', '--Iteration', action="store", type = int, help='Number of Iterations')
    exclusive_group.add_argument('-j', '--JSON_File', action="store_true", help='Read iteration value from JSON file.')
    args = parser.parse_args()
    return args



arguments = Parse_Arguments()
if arguments.JSON_File == True:
    fileDir = os.getcwd()
    fileExt = r".json"
    JSON_Filename = [_ for _ in os.listdir(fileDir) if _.endswith(fileExt)]
    # JSON file
    f = open (JSON_Filename[0], "r")
    # Reading from file
    data = json.loads(f.read())
    Estimate_pi(data['Iteration'])
    f.close()
else:
    Estimate_pi(arguments.Iteration)

