import argparse
import os
from datetime import datetime
import numpy as np

def Calculate_Stats(path, Round = 3):
    file = open (path, "r")
    data = np.array(file.read().splitlines(), dtype = int)
    print('-'*50)
    print('Date-Time:', datetime.now().strftime("%d-%m-%Y-%I-%M-%S"))
    print('Stats for Websocket Latency')
    print('-'*50)
    print('Mean:', round(np.mean(data),Round),'ns')
    print('Median:', round(np.median(data),Round),'ns')
    print('Min:', round(np.min(data),Round), 'ns')
    print('Max:', round(np.max(data),Round), 'ns')
    print('Standard Deviation:', round(np.std(data),Round), 'ns')
    print('90th percentile:', round(np.percentile(data,90),Round), 'ns')
    print('99th percentile:', round(np.percentile(data,99),Round), 'ns')
    print('99.9th percentile:', round(np.percentile(data,99.9),Round), 'ns')
    print('99.99th percentile:', round(np.percentile(data,99.99),Round), 'ns')
    print('99.999th percentile:', round(np.percentile(data,99.999),Round), 'ns')
    print('Total values:', len(data))


def main():
    parser = argparse.ArgumentParser(description='Latency Stats Calculator')
    parser.add_argument('-l', '--location', action="store", type = str, help='Path of message latency file')
    args = parser.parse_args()
    file_path = os.path.split(args.location)
    if os.path.exists(args.location):
        Calculate_Stats(args.location)

main()
