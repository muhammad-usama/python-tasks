import time
import asyncio
import pprint
import logging

logging.basicConfig(filename = 'find_dividibles_async.log',level=logging.DEBUG)
async def async_find_divisible(in_range, divisor):
    logging.debug( "find_divisibles called with range {} and divisor {}".format(in_range,divisor))
    start = time.time()
    num_list = list()
    for val in range(1,in_range):
        if val%divisor == 0:
            num_list.append(val)
        if val%10000 == 0:
            await asyncio.sleep(0)
    end = time.time()
    logging.debug( "find_divisibles ended with range {} and divisor {}. It took {} seconds".format(in_range,divisor,end - start))
    return num_list

async def main():
    task1 = loop.create_task(async_find_divisible(50800000, 34113))
    task2 = loop.create_task(async_find_divisible(100052, 3210))
    task3 = loop.create_task(async_find_divisible(500, 3))
    await asyncio.wait([task1,task2,task3])
    pprint.pprint('find_divisible(100052, 3210)')
    pprint.pprint(task2.result())
    pprint.pprint('find_divisible(500, 3)')
    pprint.pprint(task3.result())

if __name__ == '__main__':
    s = time.time()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
    e = time.time()
    logging.debug('Total time:' + str(e - s))

