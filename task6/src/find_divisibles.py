import time
import pprint
import logging

logging.basicConfig(filename = 'find_dividibles.log',level=logging.DEBUG)
def find_divisible(in_range, divisor):
    logging.debug( "find_divisibles called with range {} and divisor {}".format(in_range,divisor))
    start = time.time()
    num_list = [val for val in range(1,in_range) if val%divisor == 0]
    end = time.time()
    logging.debug( "find_divisibles ended with range {} and divisor {}. It took {} seconds".format(in_range,divisor,end - start))
    return num_list

def main():
    task1 = find_divisible(50800000, 34113)
    task2 = find_divisible(100052, 3210)
    task3 = find_divisible(500, 3)
    pprint.pprint('find_divisible(100052, 3210)')
    pprint.pprint(task2)
    pprint.pprint('find_divisible(500, 3)')
    pprint.pprint(task3)

if __name__=="__main__":
    s = time.time()
    main()
    e = time.time()
    logging.debug('Total time:' + str(e - s))