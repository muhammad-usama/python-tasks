import cpuinfo
import psutil
import os
info = cpuinfo.DataSource.lscpu()
info = info[1].split('\n')

index_list = [2,7,8,13,15,16,17,19,20,22,23]
dir_path = '/home/{}/Details'.format(os.getlogin())
if os.path.exists(dir_path) == False:
    os.mkdir(dir_path)

with open(dir_path+'/Summary.txt','w') as f:
    for i in index_list:
        f.write(info[i]+'\n')
    f.write('RAM:' + ' '*29 + str(psutil.virtual_memory().total//(1024*1024)) + ' MiB')
