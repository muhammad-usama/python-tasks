import argparse
def Parse_Argument():
    parser = argparse.ArgumentParser(description='Distance Calculator')
    parser.add_argument('Up', metavar = 'Up', type = int, help = 'Number of steps in the upward direction')
    parser.add_argument('Down', metavar = 'Down', type = int, help = 'Number of steps in the downward direction')
    parser.add_argument('Left', metavar = 'Left', type = int, help = 'Number of steps in the leftward direction')
    parser.add_argument('Right', metavar = 'Right', type = int, help = 'Number of steps in the rightward direction')
    args = parser.parse_args()
    return args

def Calculate_Distance(args):
    x = args.Right - args.Left
    y = args.Up - args.Down
    return round((x**2 + y**2)**(1/2))

def Display_Postion(args):
    print('Current Position: (0,0)')
    print('Moving {} steps in the upward direction:'.format(args.Up))
    print('Current Position: (0,{})'.format(args.Up))
    print('Moving {} steps in the downward direction:'.format(args.Down))
    print('Current Position: (0,{})'.format(args.Up - args.Down))
    print('Moving {} steps in the left direction:'.format(args.Left))
    print('Current Position: ({},{})'.format(-args.Left,args.Up - args.Down))
    print('Moving {} steps in the right direction:'.format(args.Right))
    print('Current Position: ({},{})'.format(args.Right - args.Left,args.Up - args.Down))



arguments = Parse_Argument()
distance = Calculate_Distance(arguments)
Display_Postion(arguments)
print('Distance: ', distance)
