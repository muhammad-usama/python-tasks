import asyncio
import websockets
import json
import time
import logging 
import os
import functools
import argparse
import shutil



global LOGGERS 
LOGGERS = list() 

def Create_logger(log_flag, filename,level):
    logger = logging.getLogger(filename)
    logger.setLevel(level)
    if log_flag == True:
        log_handler = logging.StreamHandler()
    else:
        print('Creating logger for {} to save data into log files.'.format(filename))
        log_handler = logging.FileHandler('./Server Log/' + filename)
    
    if level == 20:
        formatter = logging.Formatter('%(levelname)s : %(message)s')
    elif level == 10:
        formatter = logging.Formatter('%(levelname)s : %(asctime)s : %(message)s')

    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)
    return logger

def Record_log(message,flag = True,loggers = LOGGERS):
    if flag == True:
        loggers[0].info(message)
        loggers[1].debug(message)
    else:
        loggers[1].debug(message)


async def Server(websocket, path):
    Record_log('Connection Successful.')
    Record_log('Opening msg_latency.txt, Mode: Write only.')
    try:
        latency_file = open('msg_latency.txt', 'w')
        Record_log('File successfully open.')
        Record_log('Writing msg_latency.txt file.')
    except Exception as e:
        Record_log(str(e))
        
    async for message in websocket:
        data = json.loads(message)
        Record_log('Received: ' + data['method'], False)
        data['rx_time'] = time.time_ns()
        data['msg_latency'] = data['rx_time'] - data['tx_time']
        latency_file.write(str(data['msg_latency'])+'\n')
        await websocket.send(json.dumps(data))
        Record_log('Sent: ' + data['method'], False)
    Record_log('Closing msg_latency.txt.')
    Record_log('Connection Closed\n'+ '-'*30 + ' End ' + '-'*30 + '\n')
    Record_log('Waiting for new client.')
    latency_file.close()

def Parse_Arguments():
    parser = argparse.ArgumentParser(description='Server Module')

    parser.add_argument('-d', '--debug_log', action="store_true", 
                        default = False, help='Debug logging.')

    parser.add_argument('-i', '--info_log', action="store_false", 
                        default = True, help='Info logging.')
    args = parser.parse_args()
    return args

async def main():
    Record_log('Waiting for new client.')
    async with websockets.serve(Server, "localhost", 9999):
        await asyncio.Future()  # run forever


args = Parse_Arguments()
if os.path.exists('./Server Log') == True:
    shutil.rmtree('./Server Log')
os.mkdir('./Server Log')
# Creating log handler
LOGGERS.append(Create_logger(args.info_log, 'server_info.log',logging.INFO))
LOGGERS.append(Create_logger(args.debug_log, 'server_debug.log',logging.DEBUG))
asyncio.run(main())
