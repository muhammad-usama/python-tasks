import asyncio
import websockets
import json
import time
import argparse
import os
import logging

def Create_logger(log_flag, filename,level):
    logger = logging.getLogger(filename)
    logger.setLevel(level)
    if log_flag == True:
        log_handler = logging.StreamHandler()
    else:
        print('Creating logger for {} to save data into log files.'.format(filename))
        log_handler = logging.FileHandler('./Client Log/' + filename)
    
    if level == 20:
        formatter = logging.Formatter('%(levelname)s : %(message)s')
    elif level == 10:
        formatter = logging.Formatter('%(levelname)s : %(asctime)s : %(message)s')

    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)
    return logger
    
def Record_log(loggers,message,flag = True):
    if flag == True:
        loggers[0].info(message)
        loggers[1].debug(message)
    else:
        loggers[1].debug(message)


async def Client(arguments, loggers):
    async with websockets.connect("ws://localhost:9999") as websocket:
        Record_log(loggers,'Connection Successful.')
        # Open JSON file and read data
        Record_log(loggers,'Opening ' + os.path.split(arguments.location)[1] + ', Mode: Read only.')
        try:
            f_read = open(arguments.location, "r")
            Record_log(loggers,'File successfully open.')
            Record_log(loggers,'Reading Messages from file.')
        except Exception as e:
            print('Except')
            Record_log(loggers,str(e))
        data = json.loads(f_read.read())
        Record_log(loggers,'Closing ' + os.path.split(arguments.location)[1])
        f_read.close()
        # Open JSON file to write data received from server 
        Record_log(loggers,'Opening received_messages.json, Mode: Write only.')
        try:
            f_write = open (os.path.split(arguments.location)[0] + '/received_messages.json', "w")
            Record_log(loggers,'File successfully open.')
        except Exception as e:
            Record_log(loggers,str(e))
        f_write.write('[ \n')
        for d in data:
            d['tx_time'] = time.time_ns()
            await websocket.send(json.dumps(d))
            Record_log(loggers,'Sent: ' + d['method'], False)
            #f_write.write(json.dumps(await websocket.recv(), indent = 5))
            message = json.loads(await websocket.recv())
            f_write.write(' '+ str(message) +'\n')
            Record_log(loggers,'Received: ' + message['method'], False)
    f_write.write(']')
    Record_log(loggers,'Closing received_messages.json')
    Record_log(loggers,'Connection Closed.')
    f_write.close()


def main():
    parser = argparse.ArgumentParser(description='Client Module')
    parser.add_argument('-l', '--location', action="store", 
                        type = str, help='Path of client_message.json File')

    parser.add_argument('-d', '--debug_log', action="store_true", 
                        default = False, help='Debug logging.')

    parser.add_argument('-i', '--info_log', action="store_false", 
                        default = True, help='Info logging.')
    
    args = parser.parse_args()
    if os.path.exists(args.location):
        if os.path.exists('./Client Log') == False:
            os.mkdir('./Client Log')
        # Creating log handler
        loggers = list()
        loggers.append(Create_logger(args.info_log, 'client_info.log',logging.INFO))
        loggers.append(Create_logger(args.debug_log, 'client_debug.log',logging.DEBUG))
        asyncio.run(Client(args,loggers))
    else:
        Record_log(loggers,'File not Found')

main()
